/*global window */
/*global $ */
/*global console */
/*global FileTransfer */
/*global console */

var mobVar = {
    "paths": {
        "baseURL": "http://magazine.finger.com.tr/1/",
        "magazines_file": "data/magazines.json",
        "version_file": "data/version.json",
        "banner_file": "data/banner.json"
    },
    "selectors": {
        "pageAbout": '#pageAbout',
        "pageDownloaded": '#pageDownloaded',
        "pageMagazines": '#pageMagazines',
        "pageCarousel": '#pageCarousel',
        "modalProgress": "#modalProgress",
        "modalProgressTitle": "#modalProgressTitle",
        "modalProgressText": "#modalProgressText",
        "modalProgressStatus": "#modalProgressStatus",
        "modalProgressBar": "#modalProgressBar",
        "sidebarLeft": '#sidebarLeft',
        "slotMagazines": '#magazinesPageSlot',
        "slotDownloadeds": '#downloadedsPageSlot',
        "galeri": '#divGalleria',
        "banner": '#divBanner'
    },
    "files": {
        "imgType": "png",
        "coverPrefix": "magazineCover",
        "pageCoverPrefix": "issueCover",
        "pagePrefix": "page",
    },
    "db": {
        "versionKey": "mobversion",
        "downloadedsKey": "mobDownloadeds",
        "magazinesKey": "mobMagazines"
    }
};

var mobLib = {
    "print": function(txt) {
        if ($("#divDebug").length === 0) {
            window.alert(txt);
        } else {
            $("#divDebug").append(txt + "<br>");
        }
        console.log(txt);
    },

    "printError": function(err) {
        if ($("#divDebug").length === 0) {
            window.alert(err.stack);
        } else {
            $("#divDebug").append(err.stack + "<br>");
        }
        console.log(err);
    },

    "test": function() {
        mobInitializer.initApp();
    },

    "hasNetworkConnection": function() {
        try {
            var networkState = navigator.connection.type;
            if (networkState != Connection.NONE) {
                return true;
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return false;
    }
};

var mobTableCreator = {
    'tmptxt': "",
    'cellcount': 0,
    'maxcellcount': 0,
    'initialize': function(maxcellcount) {
        this.maxcellcount = maxcellcount;
        this.cellcount = 0;
    },
    'add': function(content) {
        if (this.cellcount == this.maxcellcount) {
            this.tmptxt = this.tmptxt + "</tr><tr>";
            this.cellcount = 0;
        }
        this.tmptxt = this.tmptxt + '<td style="padding:5px;">' + content + "</td>";
        this.cellcount = this.cellcount + 1;
    },
    'gethtml': function() {
        retval = '<table class="mcontent-table"><tr>' + this.tmptxt;
        for (var i = this.cellcount; i < this.maxcellcount; i++) {
            retval = retval + '<td style="padding:5px;"></td>';
        }
        retval = retval + "<tr></table>";
        this.tmptxt = "";
        return retval;
    }
};

var mobProgress = {
    "stepCount": 1,
    "stepDone": 0,
    "fn_create": function(title, stepCount) {
        try {
            title = title || "";
            mobProgress.stepCount = stepCount || 1;
            mobProgress.stepDone = 0;
            $(mobVar.selectors.modalProgressTitle).html(title);

            $(mobVar.selectors.modalProgressBar).css('width', '0%').attr('aria-valuenow', 0);
            $(mobVar.selectors.modalProgress).modal({
                backdrop: 'static',
                keyboard: false
            });
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_showText": function(text) {
        text = text || "";
        // $(mobVar.selectors.modalProgressText).html(text);

    },
    "fn_stepDone": function() {
        try {
            mobProgress.stepDone = mobProgress.stepDone + 1;
            var text = mobProgress.stepDone + "/" + mobProgress.stepCount;
            $(mobVar.selectors.modalProgressStatus).html(text);
            var perc = parseInt(mobProgress.stepDone / mobProgress.stepCount * 100);
            $(mobVar.selectors.modalProgressBar).css('width', perc + '%').attr('aria-valuenow', perc);

        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_showTextAndDone": function(text) {
        mobProgress.fn_showText(text);
        mobProgress.fn_stepDone();
    },
    "fn_finished": function() {
        $(mobVar.selectors.modalProgress).modal('hide');
    }
};

// var a = '<button id="btnindir{MID}" type="button" class="btn btn-primary btn-sm btn-read" onclick="mobEvents.fn_onDownloadClicked({MID});">İndir</button> ' +
//     '    <div id="modalProgressBar{MID}" style="display:none" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> ' +
//     '      <div id="modalProgressStatus{MID}" class="text-center"></div></div>';
var mobDownloadMag = {
    "magid": null,
    "stepCount": 0,
    "stepDone": 0,
    "fn_start": function(magid, pagecount) {
        mobDownloadMag.magid = magid;
        mobDownloadMag.stepCount = pagecount;
        $(".btn-down").hide();
        // $("#progress" + mobDownloadMag.magid).show();
        $(".progressmag").show();
        console.log("#progressBar" + mobDownloadMag.magid);
        // progressStatus
    },
    "fn_step": function() {
        mobDownloadMag.stepDone = mobDownloadMag.stepDone + 1;
        var perc = parseInt(mobDownloadMag.stepDone / mobDownloadMag.stepCount * 100);
        console.log(mobDownloadMag.stepDone + " / " + mobDownloadMag.stepCount + ">perc>" + perc);
        $("#progressBar" + mobDownloadMag.magid).css('width', perc + '%').attr('aria-valuenow', perc);
        $("#progressStatus" + mobDownloadMag.magid).html(mobDownloadMag.stepDone + " / " + mobDownloadMag.stepCount);
    },
    "fn_stop": function() {
        // $("#progress" + mobDownloadMag.magid).hide();
        $(".progressmag").hide();
        $(".btn-down").show();
        $("#btnindir" + mobDownloadMag.magid).hide();
        mobDownloadMag.magid = null;
        mobDownloadMag.stepCount = 0;
        mobDownloadMag.stepDone = 0;
    }
};

var mobCovers = {
    "removePathList": [],
    "removeCounter": -1,
    "fn_removeCoversAndCopyExists": function() {
        try {
            if (mobData.magazinesObj !== null && mobData.rmagazinesObj !== null) {
                for (var i = 0; i < mobData.magazinesObj.issues.length; i++) {
                    // eger magazine varsa local path i esitle
                    if (mobData.fn_isIdExistsInMagazinesObject(mobData.magazinesObj.issues[i].id))
                        mobData.rmagazinesObj.issues[i].localPath = mobData.magazinesObj.issues[i].localPath;
                    // yoksa remove listesine ekle
                    else mobCovers.removePathList.append(mobData.magazinesObj.issues[i].localPath);
                }
                mobCovers.removeCounter = mobCovers.removePathList.length;
                mobCovers._fn_removePathList();
            }
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_downloadCovers": function(index) {
        index = index || 0;
        try {
            if (index < mobData.rmagazinesObj.issues.length) {
                if (mobData.rmagazinesObj.issues[index].localPath === null) {
                    mobProgress.fn_stepDone();
                    //Open filesystem
                    window.requestFileSystem(window.LocalFileSystem.PERSISTENT, 5 * 1024 * 1024, function OnSuccessLoadFS(fs) {
                        var uri = mobVar.paths.baseURL + mobData.rmagazinesObj.issues[index].imagePath;
                        var filename = mobVar.files.coverPrefix + mobData.rmagazinesObj.issues[index].id + "." + mobVar.files.imgType;
                        // Create or if exists return file
                        fs.root.getFile(filename, {
                            create: true,
                            exclusive: false
                        }, function OnSuccessCreateFile(fileEntry) {
                            var fileTransfer = new FileTransfer();
                            var fileURL = fileEntry.toURL();
                            fileTransfer.download(
                                uri,
                                fileURL,
                                function OnSuccessFileTransfer(entry) {
                                    try {
                                        mobData.rmagazinesObj.issues[index].localPath = fileEntry.toURL();
                                        mobCovers.fn_downloadCovers(index + 1);
                                    } catch (err) {
                                        mobLib.printError(err);
                                    }

                                },
                                function OnErrorFileTransfer(error) {
                                    mobLib.print("Magazinler indirilirken hata: " + error.source + " ,hedef: " + error.target + " ,hata kodu: " + error.code);
                                    mobInitializer.init4CoversDownloaded(false);
                                }, null, {}
                            );
                        }, function OnErrorCreateFile(error) {
                            mobLib.print("Dosyayı kaydederken hata: " + error.code + " ,hata mesajı: " + error.message);
                            mobInitializer.init4CoversDownloaded(false);
                        });
                    }, function OnErrorLoadFs(error) {
                        mobLib.print("Dosya sistemini açarken hata: " + error.code + " ,hata mesajı: " + error.message);
                        mobInitializer.init4CoversDownloaded(false);
                    });
                } else mobCovers.fn_downloadCovers(index + 1);
            } else {
                mobInitializer.init4CoversDownloaded(true);
            }
        } catch (err) {
            mobLib.printError(err);
        }

    },
    "_fn_removePathList": function() {
        //        if (mobCovers.removeCounter > -1) {
        //            // TODO: Later
        //            //            var path = "file:///storage/emulated/0";
        //            //            var filename = "myfile.txt";
        //            //
        //            //            window.resolveLocalFileSystemURL(path, function (dir) {
        //            //                dir.getFile(filename, {
        //            //                    create: false
        //            //                }, function (fileEntry) {
        //            //                    fileEntry.remove(function () {
        //            //                        // The file has been removed succesfully
        //            //                    }, function (error) {
        //            //                        // Error deleting the file
        //            //                    }, function () {
        //            //                        // The file doesn't exist
        //            //                    });
        //            //                });
        //            //            });
        //            mobCovers.removeCounter = mobCovers.removeCounter - 1;
        //            mobCovers._fn_removePathList();
        //        } else {
        //            removePathList = [];
        //        }
        return;
    }

};

var mobIssue = {
    "_jsonObj": null,
    "_magazineId": null,
    "_downloaded": true,
    "_callbackFinished": null,
    "fn_downloadMagazine": function(magazineId, callbackFinished) {
        try {
            mobIssue._jsonObj = null;
            mobIssue._magazineId = magazineId;
            mobIssue._callbackFinished = callbackFinished;
            var jsonUrl = mobData.fn_getMagazineIssuePathById(magazineId);
            if (jsonUrl !== null) mobIssue._fn_getJson(jsonUrl);
            else mobLib.print("Magazin adresi hatalı!");
        } catch (err) {
            mobLib.printError(err);
        }

    },
    "_fn_getJson": function(jsonUrlSuffix) {
        try {
            var jsonobj = mobData.fn_getDownloadedObjById(mobIssue._magazineId);
            if (jsonobj !== null) {
                mobIssue._jsonObj = jsonobj;
            } else {
                var jsonUrl = mobVar.paths.baseURL + jsonUrlSuffix;
                $.ajax({
                    async: true,
                    dataType: 'json',
                    url: jsonUrl,
                    success: function onSuccess(result) {
                        mobIssue._jsonObj = result;
                        // mobProgress.fn_create("İndiriliyor...", mobIssue._jsonObj.pages.length + 1);
                        mobDownloadMag.fn_start(mobIssue._magazineId, mobIssue._jsonObj.pages.length + 1);
                        mobIssue._fn_getCover();
                    },
                    error: function onError(xhr, ajaxOptions, thrownError) {
                        mobLib.print("Magazin kimlik bilgilerine erişilemiyor!");
                    }
                });
            }
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "_fn_getCover": function() {
        try {
            // mobProgress.fn_showText("Kapak indiriliyor...");
            // mobProgress.fn_stepDone();
            mobDownloadMag.fn_step();
            if (mobIssue._jsonObj.localPath === null) {
                //Open filesystem
                window.requestFileSystem(window.LocalFileSystem.PERSISTENT, 5 * 1024 * 1024, function OnSuccessLoadFS(fs) {
                    try {
                        var uri = mobVar.paths.baseURL + mobIssue._jsonObj.imagePath;
                        var filename = mobVar.files.pageCoverPrefix + mobIssue._jsonObj.id + "." + mobVar.files.imgType;

                        // Create or if exists return file
                        fs.root.getFile(filename, {
                            create: true,
                            exclusive: false
                        }, function OnSuccessCreateFile(fileEntry) {
                            try {
                                // Download image
                                var fileTransfer = new FileTransfer();
                                var fileURL = fileEntry.toURL();
                                fileTransfer.download(
                                    uri,
                                    fileURL,
                                    function OnSuccessFileTransfer(entry) {
                                        try {
                                            mobIssue._jsonObj.localPath = fileEntry.toURL();
                                            mobIssue._fn_getPage(0);
                                        } catch (err) {
                                            mobLib.printError(err);
                                        }

                                    },
                                    function OnErrorFileTransfer(error) {
                                        mobLib.print("Kapak sayfasını indirirken hata: " + error.source + " ,hedef: " + error.target + " ,hata kodu: " + error.code);
                                        mobIssue._downloaded = false;
                                        mobIssue._fn_getPage(0);
                                    }, null, {}
                                );
                            } catch (err) {
                                mobLib.printError(err);
                            }
                        }, function OnErrorCreateFile(error) {
                            mobLib.print("Dosyayı kaydederken hata: " + error.code + " ,hata mesajı: " + error.message);
                            mobIssue._downloaded = false;
                            mobIssue._fn_getPage(0);
                        });
                    } catch (err) {
                        mobLib.print(err.stack);
                    }
                }, function OnErrorLoadFs(error) {
                    mobLib.print("Dosya sistemini açarken hata: " + error.code + " ,hata mesajı: " + error.message);
                    mobIssue._downloaded = false;
                    mobIssue._fn_getPage(0);
                });
            } else mobIssue._fn_getPage(0);
        } catch (err) {
            mobLib.printError(err);
        }

    },
    "_fn_getPage": function(index) {
        try {
            // mobProgress.fn_showText("Sayfalar indiriliyor...");
            // mobProgress.fn_stepDone();
            mobDownloadMag.fn_step();
            if (index >= mobIssue._jsonObj.pages.length) {
                mobIssue._jsonObj.downloaded = mobIssue._downloaded;
                // mobProgress.fn_showText("Kaydediliyor...");
                mobIssue._fn_save();
                // mobProgress.fn_finished();
                mobDownloadMag.fn_stop();
            } else {
                if (mobIssue._jsonObj.pages[index].localPath === null) {
                    //Open filesystem
                    window.requestFileSystem(window.LocalFileSystem.PERSISTENT, 5 * 1024 * 1024, function OnSuccessLoadFS(fs) {
                        var uri = mobVar.paths.baseURL + mobIssue._jsonObj.pages[index].imagePath;
                        var filename = mobVar.files.pagePrefix + mobIssue._jsonObj.id + "_" + mobIssue._jsonObj.pages[index].id + "." + mobVar.files.imgType;
                        // Create or if exists return file
                        fs.root.getFile(filename, {
                            create: true,
                            exclusive: false
                        }, function OnSuccessCreateFile(fileEntry) {
                            // Download image
                            var fileTransfer = new FileTransfer();
                            var fileURL = fileEntry.toURL();
                            fileTransfer.download(
                                uri,
                                fileURL,
                                function OnSuccessFileTransfer(entry) {
                                    try {
                                        mobIssue._jsonObj.pages[index].localPath = fileEntry.toURL();
                                        mobIssue._fn_getPage(index + 1);
                                    } catch (err) {
                                        mobLib.printError(err);
                                    }

                                },
                                function OnErrorFileTransfer(error) {
                                    mobLib.print("Sayfaları indirirken hata: " + error.source + " ,hedef: " + error.target + " ,hata kodu: " + error.code);
                                    mobIssue._downloaded = false;
                                    mobIssue._fn_getPage(index + 1);

                                }, null, {}
                            );
                        }, function OnErrorCreateFile(error) {
                            mobLib.print("Dosyayı kaydederken hata: " + error.code + " ,hata mesajı: " + error.message);
                            mobIssue._downloaded = false;
                            mobIssue._fn_getPage(index + 1);

                        });
                    }, function OnErrorLoadFs(error) {
                        mobLib.print("Dosya sistemini açarken hata " + error.code + " ,hata mesajı: " + error.message);
                        mobIssue._downloaded = false;
                        mobIssue._fn_getPage(index + 1);

                    });
                } else mobIssue._fn_getPage(index + 1);

            }
        } catch (err) {
            mobLib.printError(err);
        }

    },
    "_fn_save": function() {
        try {
            mobData.fn_saveDownloadedObj(mobIssue._jsonObj);
            if (mobIssue._callbackFinished !== null) {
                var cb = mobIssue._callbackFinished;
                mobIssue._callbackFinished = null;
                cb();
            }
        } catch (err) {
            mobLib.printError(err);
        }
    }
};

var mobData = {
    "storage": window.localStorage,
    "rVersion": -1,
    "lVersion": -1,
    "magazinesObj": null,
    "rmagazinesObj": null,
    "downloadedsObj": {
        "issues": []
    },
    "fn_downloadLastVersionNum": function() {
        try {
            var remoteUrl = mobVar.paths.baseURL + mobVar.paths.version_file;
            mobData._fn_download(remoteUrl, mobData._onSuccessDownloadVersion, mobData._onErrorDownloadVersion);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_downloadRmagazinesObject": function() {
        try {
            var remoteUrl = mobVar.paths.baseURL + mobVar.paths.magazines_file;
            mobData._fn_download(remoteUrl, mobData._onSuccessDownloadMagazines, mobData._onErrorDownloadMagazines);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_readLocalData": function() {
        try {
            // read version
            var version = mobData.storage.getItem(mobVar.db.versionKey);
            if (isNaN(version) || version === null) mobData.lVersion = -1;
            else mobData.lVersion = parseInt(version);

            // read downloadeds
            var djson = mobData.storage.getItem(mobVar.db.downloadedsKey);
            if (djson) mobData.downloadedsObj = JSON.parse(djson);
            else mobData.downloadedsObj = null;

            // read magazines

            var mjson = mobData.storage.getItem(mobVar.db.magazinesKey);
            if (mjson) mobData.magazinesObj = JSON.parse(mjson);
            else mobData.magazinesObj = null;
        } catch (err) {
            mobLib.printError(err);
        }


    },
    "_fn_download": function(remoteUrl, success_callback_function, errorCallbackFunction) {
        $.ajax({
            async: true,
            dataType: 'json',
            url: remoteUrl,
            success: success_callback_function,
            error: errorCallbackFunction
        });
    },
    "_onSuccessDownloadVersion": function(result) {
        try {
            mobData.rVersion = parseInt(result.version);
            mobInitializer.init1VersionDownloaded();
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "_onErrorDownloadVersion": function(xhr, ajaxOptions, thrownError) {
        mobInitializer.init1VersionDownloaded();
        mobLib.print("İndirme  sırasında bir hata oluştu: " + xhr.status + " " + xhr.statusText + " Hata:" + thrownError);
    },
    "_onSuccessDownloadMagazines": function(result) {
        try {
            mobData.rmagazinesObj = result;
            mobInitializer.init2RmagazineDownloaded();
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "_onErrorDownloadMagazines": function(xhr, ajaxOptions, thrownError) {
        mobInitializer.init2RmagazineDownloaded();
        mobLib.print("İndirme  sırasında bir hata oluştu: " + xhr.status + " " + xhr.statusText + " Hata:" + thrownError);
    },
    "fn_saveVersion": function() {
        try {
            mobData.storage.setItem(mobVar.db.versionKey, mobData.lVersion);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_saveMagazines": function() {
        try {
            var magob = JSON.stringify(mobData.magazinesObj);
            mobData.storage.setItem(mobVar.db.magazinesKey, magob);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_saveDownloadeds": function() {
        try {
            var dawnob = JSON.stringify(mobData.downloadedsObj);
            mobData.storage.setItem(mobVar.db.downloadedsKey, dawnob);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_isIdExistsInMagazinesObject": function(itemid) {
        try {
            if (mobData.magazinesObj !== null) {
                var maxl = mobData.magazinesObj.issues.length;
                for (var i = 0; i < maxl; i++) {
                    if (mobData.magazinesObj.issues[i].id == itemid) return true;
                }
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return false;
    },
    "fn_isIdExistsInDownloadedsObject": function(itemid) {
        try {
            if (mobData.downloadedsObj !== null) {
                var maxl = mobData.downloadedsObj.issues.length;
                for (var i = 0; i < maxl; i++) {
                    if (mobData.downloadedsObj.issues[i].id == itemid) return true;
                }
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return false;
    },
    "fn_getMagazineIssuePathById": function(magazineId) {
        try {
            for (var i = 0; i < mobData.magazinesObj.issues.length; i++) {
                if (parseInt(mobData.magazinesObj.issues[i].id) == parseInt(magazineId)) return mobData.magazinesObj.issues[i].issuePath;
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return null;
    },
    "fn_getDownloadedObjById": function(magazineId) {
        var retval = null;
        try {
            if (mobData.downloadedsObj !== null) {
                for (var i = 0; i < mobData.downloadedsObj.issues.length; i++) {
                    if (mobData.downloadedsObj.issues[i].id == magazineId) {
                        retval = mobData.downloadedsObj.issues[i];
                        break;
                    }
                }
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return retval;
    },
    "fn_saveDownloadedObj": function(issue) {
        try {
            if (mobData.downloadedsObj !== null) {
                if (mobData.fn_getDownloadedObjById(issue.id) !== null) {
                    for (var i = 0; i < mobData.downloadedsObj.issues.length; i++) {
                        if (mobData.downloadedsObj.issues[i].id == issue.id) {
                            mobData.downloadedsObj.issues[i] = issue;
                            break;
                        }
                    }
                } else {
                    mobData.downloadedsObj.issues.push(issue);
                }
            } else {
                mobData.downloadedsObj = {
                    "issues": []
                };
                mobData.downloadedsObj.issues.push(issue);
            }
            mobData.fn_saveDownloadeds();
        } catch (err) {
            mobLib.printError(err);
        }

    },
    "fn_removeFile": function(filepath, onsucess_function, onfail_function) {
        //        TODO:  Fill in the blanks
        // function fail(error)
        // function success(entry)
        //        fileSystem.root.getFile(
        //            filepath, {
        //                create: false,
        //                exclusive: false
        //            },
        //            function onSuccessGetFile() {
        //                fileEntry.remove(onsucess_function, onfail_function);
        //            },
        //            onfail_function
        //        );
    },
    "fn_getMagazineIndexById": function(magazineId) {
        try {
            for (var i = 0; i < mobData.magazinesObj.issues.length; i++) {
                if (parseInt(mobData.magazinesObj.issues[i].id) == parseInt(magazineId)) return i;
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return null;
    }
};

var mobPages = {
    "fn_constructPages": function() {
        mobPages.fn_constructMagazinesPage();
        mobPages.fn_constructDownloadedsPage();
    },
    "fn_constructMagazinesPage": function() {
        try {
            // '<div class="thumbnail floating-box thumbnail-cover"> ' +
            var mtemp = '<img src="{IMG_PATH}" width="100%"> ' +
                '<div class="caption"> ' +
                '<p class="text-center">{TEXT}</p> ' +
                '<p class="text-center"> ' +
                '  <button id="btnindir{MID}" type="button" class="btn btn-primary btn-sm btn-read btn-down" onclick="mobEvents.fn_onDownloadClicked({MID});">İndir</button> ' +
                '</p>' +
                '<p class="text-center ctm">' +
                '   <div id="progress{MID}" class="progress progressmag" style="display: none;"> ' +
                '       <div id="progressBar{MID}" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> ' +
                '           <div id="progressStatus{MID}" class="text-center"></div>' +
                '       </div>' +
                '   </div>' +
                '</p>' +
                '</div>';
            //</div>

            mobTableCreator.initialize(3);

            if (mobData.magazinesObj !== null) {
                for (var i = 0; i < mobData.magazinesObj.issues.length; i++) {
                    var mid = mobData.magazinesObj.issues[i].id;
                    var imgPath = mobData.magazinesObj.issues[i].localPath;
                    var text = mobData.magazinesObj.issues[i].text;
                    if (!mobData.fn_isIdExistsInDownloadedsObject(mid)) {
                        var itemp = mtemp;
                        // replace replaces only first match
                        itemp = itemp.replace("{IMG_PATH}", imgPath).replace("{TEXT}", text).replace("{MID}", mid).replace("{MID}", mid).replace("{MID}", mid).replace("{MID}", mid).replace("{MID}", mid);
                        mobTableCreator.add(itemp);
                        totalhtml += itemp;
                    }
                }
            }
            var totalhtml = mobTableCreator.gethtml();
            $(mobVar.selectors.slotMagazines).html(totalhtml);
        } catch (err) {
            mobLib.printError(err);
        }
    },
    "fn_constructDownloadedsPage": function() {
        try {
            // '<div class="thumbnail floating-box thumbnail-cover">' +
            var dtemp = '  <img src="{IMG_PATH}" width="100%">' +
                // '  <img src="{IMG_PATH}" width="50%">' +
                '  <div class="caption">' +
                '   <p class="text-center">{TEXT}</p>' +
                '   <p class="text-center">' +
                '     <button id="btnoku_{DID}" type="button" class="btn btn-default btn-sm btn-read btnOku" ' +
                '       onclick="mobEvents.fn_onReadClicked({DID});">Oku</button>' +

                '   </p>' +
                '  </div>' +
                '</div>';
            mobTableCreator.initialize(3);
            if (mobData.downloadedsObj !== null) {
                for (var i = 0; i < mobData.downloadedsObj.issues.length; i++) {
                    var did = mobData.downloadedsObj.issues[i].id;
                    var imgPath = mobData.downloadedsObj.issues[i].localPath;
                    var text = mobData.downloadedsObj.issues[i].text;
                    var itemp = dtemp;
                    itemp = itemp.replace("{IMG_PATH}", imgPath).replace("{TEXT}", text).replace("{DID}", did).replace("{DID}", did);
                    mobTableCreator.add(itemp);
                }

                var totalhtml = mobTableCreator.gethtml();
                $(mobVar.selectors.slotDownloadeds).html(totalhtml);
            }
        } catch (err) {
            mobLib.printError(err);
        }
        return;
    }

};

var mobBanner = {

    "bannerList": [],
    "fn_createBanner": function() {
        $.ajax({
            async: true,
            dataType: 'json',
            url: mobVar.paths.baseURL + mobVar.paths.banner_file,
            success: function success_callback_function(result) {
                try {
                    var banner = $(mobVar.selectors.banner);
                    tempContent = '<div class="item"><img src="{SRC}"/></div>';
                    var owlContent = "";
                    for (var i = 0; i < result.banners.length; i++) {
                        mobBanner.bannerList.push(result.banners[i].url);
                        owlContent = owlContent + tempContent.replace('{SRC}', result.banners[i].url);
                    }
                    banner.html(owlContent);
                    banner.owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: false,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 3
                            },
                            1000: {
                                items: 5
                            }
                        }
                    });
                } catch (err) {
                    mobLib.printError(err);
                }

            },
            error: function errorCallbackFunction(xhr, ajaxOptions, thrownError) {
                mobLib.print("İndirme  sırasında bir hata oluştu: " + xhr.status + " " + xhr.statusText + " Hata:" + thrownError);
            }
        });

    }
};

var mobInitializer = {
    "initApp": function() {
        mobData.fn_readLocalData();
        if (mobLib.hasNetworkConnection()) {
            mobBanner.fn_createBanner();
            mobData.fn_downloadLastVersionNum();
        } else {
            mobLib.print("İnternet bağlantısı bulunamadı!");
            mobPages.fn_constructPages();
        }
    },
    "init1VersionDownloaded": function() {
        if (mobData.lVersion < mobData.rVersion) {
            mobData.fn_downloadRmagazinesObject();
        } else mobPages.fn_constructPages();
    },
    "init2RmagazineDownloaded": function() {
        if (mobData.rmagazinesObj !== null) mobCovers.fn_removeCoversAndCopyExists();
        mobProgress.fn_create("İndiriliyor...", mobData.rmagazinesObj.issues.length);
        mobProgress.fn_showText("Güncelleniyor...");
        mobCovers.fn_downloadCovers();
    },
    "init4CoversDownloaded": function(isSuccess) {
        mobProgress.fn_finished();
        if (isSuccess) {
            mobData.lVersion = mobData.rVersion;
            mobData.rVersion = -1;
            mobData.magazinesObj = mobData.rmagazinesObj;
            mobData.rmagazinesObj = null;
            mobData.fn_saveVersion();
            mobData.fn_saveMagazines();
            mobPages.fn_constructPages();
        }
        mobPages.fn_constructPages();
    }

};

var mobEvents = {
    "fn_onInit": function() {
        mobInitializer.initApp();
    },
    "fn_onDownloadClicked": function(itemId) {
        if (mobLib.hasNetworkConnection()) {
            mobIssue.fn_downloadMagazine(itemId, mobPages.fn_constructPages);
        } else {
            mobLib.print("İnternet bağlantısı yok");
        }
    },
    "fn_onReadClicked": function(itemid) {
        try {
            var downloadedItem = mobData.fn_getDownloadedObjById(itemid);
            var pswpElement = document.querySelectorAll('.pswp')[0];
            if (downloadedItem !== null) {
                var winWidth = $(window).width();
                var winHeight = $(window).height();
                var items = [];
                for (var i = 0; i < downloadedItem.pages.length; i++) {
                    items.push({
                        src: downloadedItem.pages[i].localPath,
                        w: winWidth,
                        h: winHeight
                    });
                }
                var options = {
                    index: 0,
                    errorMsg: "Sayfa bulunamadı!"
                };

                // Initializes and opens PhotoSwipe
                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();

            } else {
                mobLib.print("İndirilen dosya bulunamadı!");
            }
        } catch (err) {
            mobLib.printError(err);
        }
    }

};


$(document).ready(function() {
    $(".menu-close").on('click', function(e) {
        // e.preventDefault();
        // $("#wrapper").toggleClass("toggled");
        $("#ocMenu").offcanvas('hide');
        // alert("aaa");

    });
});
