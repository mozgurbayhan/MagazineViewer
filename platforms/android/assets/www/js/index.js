// FOR DISABLING CACHE
$.ajaxSetup({
 cache: false
});
// - FOR DISABLING CACHE
//
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

        // DISABLE ROTATION
        window.addEventListener("orientationchange", function() {
            screen.lockOrientation('portrait');
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        mobInitializer.initApp();
    },


};

app.initialize();

$(document).ready(function() {
    // About left menu
});
