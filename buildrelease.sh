#!/bin/bash

JAVABINPATH=/usr/lib/jvm/java-8-oracle/bin
ANDBUILDTOOLSPATH=/home/ozgur/mount/media/LinuxDepo/ozgurDepo/Android/SDK/build-tools/26.0.2
UNSIGNEDAPK=/home/ozgur/Desktop/myworks/workspaces/cordovaws/MagazineViewer/platforms/android/build/outputs/apk/android-release-unsigned.apk
KEYTOOL=${JAVABINPATH}/keytool
JARSIGNER=${JAVABINPATH}/jarsigner
ZIPALIGN=${ANDBUILDTOOLSPATH}/zipalign




# echo " >> Removing Cordova console plugin"
# cordova plugin rm cordova-plugin-console
echo "MOB >> creating key : 123467"
${KEYTOOL} -genkey -v -keystore mviewer.keystore -alias dosyaadi -keyalg RSA -keysize 2048 -validity 10000
echo "MOB >> Building For android release"
cordova build --release android
echo "MOB >> signing apk"
${JARSIGNER} -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore mviewer.keystore ${UNSIGNEDAPK} dosyaadi
echo "MOB >> zipalign apk"
rm MagazineViewer.apk
${ZIPALIGN} -v 4 ${UNSIGNEDAPK} MagazineViewer.apk
# cordova plugin install cordova-plugin-console
echo "MOB >> DONE!"

# JAVABINPATH=/usr/lib/jvm/java-8-oracle/bin
# echo "MOB >> creating key : 123467"
# ${JAVABINPATH}/keytool -genkey -alias samplekey -v -keystore samplekey.keystore -keyalg RSA -keysize 2048 -validity 10000
# echo "MOB >> Building For android release"
# cordova build android --release
# echo "MOB >> signing apk"
# ${JAVABINPATH}/jarsigner  -verbose -keystore samplekey.keystore -storepass 123467 -keypass 123467 -signedjar ./signed.apk /home/ozgur/Desktop/myworks/workspaces/cordovaws/MagazineViewer/platforms/android/build/outputs/apk/android-release-unsigned.apk samplekey
# echo "MOB >> verifying"
# ${JAVABINPATH}/jarsigner -verify -verbose -certs signed.apk
