# README #

* Magazine Viewer Mobile Application
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

**TECH**

* [Cordova ](https://cordova.apache.org)

* Javascript

* [Bootstrap 3](http://getbootstrap.com)

* [Simple Sidebar](https://startbootstrap.com/template-overviews/simple-sidebar/)

* [Photoswipe](http://photoswipe.com)

**DOCS**

* [Cordova](https://cordova.apache.org/docs/en/latest/guide/cli/index.html)

# TODO (Test after every version)#

**v0.1**

DONE: Create Bare Project By Cordova

DONE: Setup bash scripts

DONE: Organize plugins

**v0.2**

DONE: Add Bootstrap

DONE: Create viewport, left menu and base pages

DONE: Copy static material

**v0.3**

DONE: Close menu on item click

DONE: Create Image Gallery -> photoswipe.com or jquery zoom + bootstrap carousel

**v0.4**

DONE: ternjs added

DONE: Fill pages Statically

DONE: Create Banner 1110x370 image size fix

DONE: Make bootstrap theme concept

**v0.5**

DONE: Import simple python server and other stuff for real world tests

DONE: Import js and other stuff from previous project

DONE: Bind engine.js

DONE: Bind image gallery

DONE: Fetch banner images json

**v0.6**

DONE: Try catch coverage

DONE: Turkish Error messages

**v0.7**

DONE: Check internet connection

DONE: no connection > pass download covers

DONE: no connection > pass download magazine

DONE: no connection > pass create banner

**v0.8**

DONE: Setup project knowledge  

DONE: Android release

**v0.9**

DONE: ana ekran açılınca slay yeri üste ve alt kısımda katologlar olucak

DONE: hakkımızdaki slyat kalkıcak

DONE: yatay ekran olmayacak

DONE: ana ekran geçiş yaparken slide sapıtıyor

DONE: dowload kısmında progress bar olucak kataloğun altında

DONE: Resimler 2 adet

DONE: sol menüde en üste logo

DONE: indirlenler ve ve indirilmeyenler aynıyerde olcuak

DONE: indirilmeyenler en üste olsun (en yeniler en üste)

DONE: ana ekrandaki padding i al

**v1.0**

TODO: Remove Magazines

DONE: save version

DONE: save magazines

DONE: save downloadeds

**v1.1**

TODO: IOS release

TODO: write docs

**v1.2**

TODO: Set up push notifications
